package custom.javax.validation;

import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class MustNotMatchValidatorTests {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void noConstraintViolationsOn_validInput() {
        RequestBean requestBean = new RequestBean();
        requestBean.setValue("The quick brown fox");
        Set<ConstraintViolation<RequestBean>> constraintViolations = validator.validate(requestBean);

        assertThat(constraintViolations.size()).isEqualTo(0);
    }

    @Test
    public void constraintViolationOn_invalidInput() {
        RequestBean requestBean = new RequestBean();
        requestBean.setValue("The quick abcdef#1 brown fox");
        Set<ConstraintViolation<RequestBean>> constraintViolations = validator.validate(requestBean);

        assertThat(constraintViolations.size()).isEqualTo(1);

        requestBean.setValue("The quick FOOBAR#01 brown fox");
        constraintViolations = validator.validate(requestBean);

        assertThat(constraintViolations.size()).isEqualTo(1);
    }

    @Data
    public class RequestBean {
        @MustNotMatch(
                pattern = @javax.validation.constraints.Pattern(
                        message = "Your submission may not contain tokens like TODO",
                        regexp = "(.*)([a-zA-Z]+#\\d{1,4})(.*)"
                )
        )
        private String value;
    }
}
