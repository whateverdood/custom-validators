package custom.javax.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class MustNotMatchValidator implements ConstraintValidator<MustNotMatch, String> {

    private Pattern pattern;

    @Override
    public void initialize(MustNotMatch constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        String regex = constraintAnnotation.pattern().regexp();
        pattern = Pattern.compile(regex);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !pattern.matcher(value).matches();
    }
}
