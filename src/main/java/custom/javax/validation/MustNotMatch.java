package custom.javax.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = MustNotMatchValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface MustNotMatch {

    String message() default "Your input contains a forbidden term.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Pattern pattern();

}
